
import { environment } from '@environment/environment';
import { Environments } from '@environment/environment-definition';
import bodyParser from 'body-parser';
import express, { Express, Request, Response } from 'express';
import morgan from 'morgan';
import winston from 'winston';
import { createLoggerConfig } from './logger';
import { RoutesProvider } from './routes-provider';
import { CAIUtils } from './v1/client/cai/util/cai.util';
import { V1 } from './v1/v1';

const gitVersion = require('../git-version.json');

class App extends RoutesProvider {
  public express: Express;

  constructor() {
    super();
  }

  protected mountRoutes(): void {
    const API_CATEGORY = 'API';
    const logger = winston.loggers.add(API_CATEGORY, createLoggerConfig(API_CATEGORY))
    .child({ process: 'mountRoutes' });
    const compression = require('compression');

    this.express = express();
    this.express.use(bodyParser.json({ limit: '10mb' }));

    if (environment.environment === Environments.LOCAL) {
      this.express.use(compression());
    }
    this.express.disable('x-powered-by');

    if (environment.environment === Environments.LOCAL) {
      this.express.use(morgan(':date[iso] :method :url :status :response-time ms - :res[content-length] :referrer'));
    }

    // express-winston logger makes sense BEFORE the router
    // this.express.use(logger({
    //   transports: [
    //     new winston.transports.Console()
    //   ],
    //   format: winston.format.combine(
    //     winston.format.colorize(),
    //     winston.format.simple()
    //   )
    // }));

    this.express.use('/v1', new V1().getRouter());

    this.router.get('/', (req: Request, res: Response): object => {
      return CAIUtils.returnMessage(res, { message: 'spotcoaching api services' });
    });

    this.router.get('/version', (req: Request, res: Response): void => {
      let version: string;

      if (environment.environment === Environments.PROD) {
        version = gitVersion.tag;
      } else {
        version = gitVersion.semverString;
      }

      res.send(`Environment: ${environment.environment.toString()} | Version: ${version}`);
    });

    /**
     * Endpoint which gets called if an error occurs in the nlp.
     * Logs the error and writes information to the database.
     */
    this.router.post(
      '/errors',
      async (req: Request, res: Response): Promise<object> => {
        logger.error('cai error occured', req.body);
        const body: any = CAIUtils.initBody(req);
        // const conversationDoc: FirebaseFirestore.DocumentData = (
        //   await (
        //     await firebaseCore.firestore.doc(`${FirestoreEndpoints.conversation.path}/${req.body.conversation.id}`)
        //   ).get()
        // ).data();
        // const auth_origin: string = conversationDoc.auth_origin;
        // const auth_userid: string = conversationDoc.auth_userid;

        // const data: FirebasetypeCAIError = {
        //   createdAt: firestore.Timestamp.now(),
        //   auth_origin,
        //   auth_userid,
        //   account: accountRef,
        //   contract: contractRef,
        //   organisation: organisationRef,
        //   code: req.body.code,
        //   level: req.body.level,
        //   data: req.body.data,
        //   conversation: req.body.conversation
        // };
        // await firebaseCore.firestore.collection(FirestoreEndpoints.error.path).add(data);

        return CAIUtils.returnMessage(res, body);
      }
    );

    this.express.use(this.getRouter());

    // express-winston errorLogger makes sense AFTER the router.
    // this.express.use(errorLogger({
    //   transports: [
    //     new winston.transports.Console()
    //   ],
    //   format: winston.format.combine(
    //     winston.format.colorize(),
    //     winston.format.simple(),
    //     winston.format.json()
    //   )
    // }));
  }
}

export default new App().express;
