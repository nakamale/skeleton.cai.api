import { EnvironmentDefinition, Environments } from '@environment/environment-definition';

export const environment: EnvironmentDefinition = {
  environment: Environments.LOCAL,
  url: 'https://api.spotcoaching.rhyno.ch',
  port: 8000,
  firebase: {
    credential: require(`../credentials/TODO.json`),
    dbUrl: 'https://hrc-sophie-dev.firebaseio.com',
    projectId: 'hrc-sophie-dev',
    storageBucket: 'hrc-sophie-dev.appspot.com'
  },
}
