
import { defaultLogger } from '@/logger';
import { environment } from '@environment/environment';
import { Server } from 'http';
import app from './app';

const server = app.listen({ port: process.env.PORT || environment.port }, (): void => {
  defaultLogger.info(`Server ready at http://localhost:${process.env.PORT || environment.port}`);
});

if (module.hot) {
  module.hot.accept();
  module.hot.dispose((): Server => server.close());
}
