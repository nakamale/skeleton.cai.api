import i18n from '@i18n/i18n.config';
import express, { Request, Response } from 'express';
import { System } from './system';
import { CAIUtils } from './util/cai.util';


export class Recast {
  private router: any;

  constructor() {
    this.router = express.Router();
    this.router.use(i18n.init);
    this.mountRoutes();
  }

  private mountRoutes(): void {
    this.router.get('/', (req: Request, res: Response) => {
      CAIUtils.logInfo(req);
      CAIUtils.returnMessage(res, { message: 'spotcoaching cai api services' });
    });

    // *************************************************************
    // General
    // *************************************************************
    this.router.post('/version', (req: Request, res: Response) => {
      new System().version(req).then((data: any) => {
        CAIUtils.returnMessage(res, data);
      });
    });

    this.router.post('/fallback', (req: Request, res: Response) => {
      new System().logFallback(req).then((data: any) => {
        CAIUtils.returnMessage(res, data);
      });
    });

    this.router.post('/detectNumber', (req: Request, res: Response) => {
      CAIUtils.detectNumber(req).then((data: any) => {
        CAIUtils.returnMessage(res, data);
      });
    });
  }

  public getRouter(): any {
    return this.router;
  }
}
