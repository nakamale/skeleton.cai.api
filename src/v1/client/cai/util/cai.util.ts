import { createLoggerConfig } from '@/logger';
import { Request } from 'express';
import winston from 'winston';
import { Memoryfields } from '@client/cai/model/memoryfields-enum';

const CAI_CATEGORY = 'CAI';
export const caiLogger = winston.loggers.add(CAI_CATEGORY, createLoggerConfig(CAI_CATEGORY));

export interface CAIBody {
  replies: any[];
  conversation: {
    memory: any;
    id: string;
  };
}

export class CAIUtils {
  /**
   * Clears all Memoryfields of the specified modules.
   * @param modules array of module-codes (eg. 'exp' for all expense-fields)
   * @param memory conversation memory
   * @returns conversation memory
   */
  public static clearMemory(modules: Array<string>, memory: any): any {
    const keys = Object.keys(memory);
    const newMemory = {};
    for (const key of keys) {
      if (modules.findIndex((mod: string) => key.startsWith(mod)) === -1) {
        newMemory[key] = memory[key];
      }
    }
    return newMemory;
  }

  public static initBody(req: any): CAIBody {
    const logger = caiLogger.child({ process: 'CAIUtils - InitBody' });
    logger.verbose('Memory');
    logger.silly(req.body.conversation.memory);

    return {
      replies: [],
      conversation: {
        memory: req.body.conversation.memory,
        id: req.body.conversation.id
      }
    };
  }

  public static reply(body: CAIBody): CAIBody {
    const logger = caiLogger.child({ process: 'CAIUtils - Reply' });
    logger.verbose('replies');
    logger.silly(body.replies);

    return body;
  }

  public static returnMessage(res, data) {
    const logger = caiLogger.child({ process: 'CAIUtils - ReturnMessage' });
    logger.verbose('returnMessage');
    logger.silly(data);

    return res.json(data);
  }

  public static logInfo(data) {
    const logger = caiLogger.child({ process: 'CAIUtils - LogInfo' });
    logger.verbose('log');
    logger.silly(data);
  }

  /**
   * Webhook which detects numbers in the inputstring.
   */
  public static async detectNumber(req: Request) {
    const logger = caiLogger.child({ process: 'CAIUtils - DetectNumber' });
    logger.info('Start');
    logger.silly('%o', { req });

    const body: any = CAIUtils.initBody(req);
    if (req.body.nlp.entities.number) {
      if (isNaN(Number(req.body.nlp.entities.number[0].raw))) {
        body.conversation.memory[Memoryfields.inputNumber] = req.body.nlp.entities.number[0].scalar;
      } else {
        body.conversation.memory[Memoryfields.inputNumber] = Number(req.body.nlp.entities.number[0].raw);
      }
    } else {
      body.conversation.memory[Memoryfields.inputNumber] = CAIUtils.detectNumberFromString(
        body.conversation.memory[Memoryfields.inputNumber]
      );
    }
    logger.info(`detecting number skill: %o`, body.conversation.memory[Memoryfields.inputNumber]);
    return CAIUtils.reply(body);
  }

  /**
   * Tries to detect the first number in a string. Returns null if no number has been detected.
   * @param inputString a string
   */
  public static detectNumberFromString(inputString: string): number {
    let res: number = null;
    try {
      const numbers: number[] = inputString.match(/[+-]?\d+(\.\d+)?/g).map(Number);
      if (numbers.length >= 1) {
        res = numbers[0];
      }
    } catch (error) {
      // do nothing
    }
    return res;
  }
}
